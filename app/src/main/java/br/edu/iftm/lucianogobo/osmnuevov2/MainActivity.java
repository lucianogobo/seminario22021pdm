package br.edu.iftm.lucianogobo.osmnuevov2;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayControlView;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<OverlayItem> pontos = new ArrayList<>();
    private MapView myOpenMapView;
    private MapController myMapController;
    private GeoPoint posicaoAtual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(tenhoPermissaoEscrito()){
            carregarMapas();
        }
    }

    private void carregarMapas(){
        GeoPoint uberaba = new GeoPoint(-19.748421, -47.936251);

        myOpenMapView = (MapView) findViewById(R.id.openmapview);
        myOpenMapView.setBuiltInZoomControls(true);
        myMapController = (MapController) myOpenMapView.getController();
        myMapController.setCenter(uberaba);
        myMapController.setZoom(6);

        myOpenMapView.setMultiTouchControls(true);

        // Focar o centro do mapa
        final MyLocationNewOverlay myLocationoverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(getApplicationContext()),myOpenMapView);
        myOpenMapView.getOverlays().add(myLocationoverlay);
        myLocationoverlay.enableMyLocation();
        myLocationoverlay.runOnFirstFix(new Runnable() {
            @Override
            public void run() {
                myMapController.animateTo(myLocationoverlay.getMyLocation());
            }
        });

        //Adicionar um ponto no mapa

        pontos.add(new OverlayItem("Uberaba","Terra do Zebu",uberaba));
        apagaPontos();

        //Detectar mudança de direção mediante uma lista
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        OSMUpdateLocation detectaPosicao = new OSMUpdateLocation(this);
        if(tenhoPermissaoDirecao()){
            Location ultimaPosicaoConhecida = null;
            for(String provider: locationManager.getProviders(true)){
                if(Build.VERSION.SDK_INT>=23 && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED)
                    ultimaPosicaoConhecida=locationManager.getLastKnownLocation(provider);
                if(ultimaPosicaoConhecida!=null){
                    atualizaPosicaoAtual(ultimaPosicaoConhecida);
                }
                //Pedir novas direções
                locationManager.requestLocationUpdates(provider,0,0,detectaPosicao);
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent();
            intent.setClass(this, this.getClass());
            startActivity(intent);
            finish();
        } else {
            // El usuario no ha dado permiso
        }
    }

    public boolean tenhoPermissaoDirecao(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
                return false;
            }
        } else {
            return true;
        }
    }

    public void atualizaPosicaoAtual(Location location){
        posicaoAtual = new GeoPoint(location.getLatitude(),location.getLongitude());
        myMapController.setCenter(posicaoAtual);
        if(pontos.size()>1){
            pontos.remove(1);
        }
        OverlayItem marcador = new OverlayItem("Esta aqui","Posicao atual",posicaoAtual);
        marcador.setMarker(ResourcesCompat.getDrawable(getResources(),R.drawable.center,null));
        pontos.add(marcador);
        apagaPontos();
    }


    public boolean tenhoPermissaoEscrito(){
        if(Build.VERSION.SDK_INT>=23){
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
                return true;
            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                return false;
            }
        }else{
            return true;
        }
    }



    private void apagaPontos(){
        myOpenMapView.getOverlays().clear();
        ItemizedIconOverlay.OnItemGestureListener<OverlayItem> tap = new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>(){

            @Override
            public boolean onItemSingleTapUp(int index, OverlayItem item) {
                return true;
            }

            @Override
            public boolean onItemLongPress(int index, OverlayItem item) {
                return false;
            }
        };
        ItemizedOverlayWithFocus<OverlayItem> capa = new ItemizedOverlayWithFocus<OverlayItem>(this,pontos,tap);
        capa.setFocusItemsOnTap(true);
        myOpenMapView.getOverlays().add(capa);
    }
}