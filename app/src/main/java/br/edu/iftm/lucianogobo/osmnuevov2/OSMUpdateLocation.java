package br.edu.iftm.lucianogobo.osmnuevov2;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class OSMUpdateLocation implements LocationListener {
    private MainActivity atividade;

    public OSMUpdateLocation(MainActivity actividad) {
        this.atividade = actividad;
    }

    @Override
    public void onLocationChanged(Location location) {
        atividade.atualizaPosicaoAtual(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
